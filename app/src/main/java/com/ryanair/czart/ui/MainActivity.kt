package com.ryanair.czart.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.ryanair.czart.R
import com.ryanair.czart.common.BaseActivity
import com.ryanair.czart.databinding.ActivityMainBinding
import com.ryanair.czart.helpers.*
import com.ryanair.czart.ui.searchResultsActivity.SearchResultsActivity
import com.ryanair.czart.ui.viewModel.MainActivityViewModel
import com.ryanair.czart.ui.viewModel.MainActivityViewModelFactory
import timber.log.Timber
import javax.inject.Inject


class MainActivity : BaseActivity() {

    @Inject
    lateinit var mainActivityViewModelFactory: MainActivityViewModelFactory
    lateinit var viewModel: MainActivityViewModel

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this, mainActivityViewModelFactory).get(MainActivityViewModel::class.java)

        /* setup binding */
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.let {
            it.lifecycleOwner = this
            it.viewModel = this.viewModel
        }
    }

    /**
     * Open results activity after button click.
     *
     * TODO all of the data should be set on the view model to make it survive configuration changes and separate it from activity.
     */
    fun search(view : View){
        val intent = Intent(this, SearchResultsActivity::class.java)
        intent.putExtras(bundleOf(
            ORIGIN to (binding.originStation.selectedItem as Station).code,
            DESTINATION to (binding.destinationStation.selectedItem as Station).code,
            DEPARTURE_DATE to "${binding.departureDate.year}-${binding.departureDate.month+1}-${binding.departureDate.dayOfMonth}",
            NUMBER_OF_ADULTS to 1,
            NUMBER_OF_CHILDREN to 0,
            NUMBER_OF_TEEN to 0
        ))
        startActivity(intent)
    }
}
