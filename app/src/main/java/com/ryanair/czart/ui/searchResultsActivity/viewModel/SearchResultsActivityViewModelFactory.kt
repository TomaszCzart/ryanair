package com.ryanair.czart.ui.searchResultsActivity.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject


class SearchResultsActivityViewModelFactory @Inject constructor(private var viewModel: SearchResultsActivityViewModel) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchResultsActivityViewModel::class.java)) {
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name " + modelClass)
    }
}