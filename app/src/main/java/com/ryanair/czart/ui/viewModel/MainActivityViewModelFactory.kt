package com.ryanair.czart.ui.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject


class MainActivityViewModelFactory @Inject constructor(private var viewModel: MainActivityViewModel) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name " + modelClass)
    }
}