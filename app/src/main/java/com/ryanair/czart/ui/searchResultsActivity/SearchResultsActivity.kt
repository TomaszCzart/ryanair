package com.ryanair.czart.ui.searchResultsActivity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.ryanair.czart.R
import com.ryanair.czart.common.BaseActivity
import com.ryanair.czart.databinding.ActivitySearchResultsBinding
import com.ryanair.czart.helpers.*
import com.ryanair.czart.ui.searchResultsActivity.recyclerView.SearchResultItem
import com.ryanair.czart.ui.searchResultsActivity.recyclerView.SearchResultsAdapter
import com.ryanair.czart.ui.searchResultsActivity.viewModel.SearchResultsActivityViewModel
import com.ryanair.czart.ui.searchResultsActivity.viewModel.SearchResultsActivityViewModelFactory
import timber.log.Timber
import javax.inject.Inject

class SearchResultsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: SearchResultsActivityViewModelFactory
    lateinit var viewModel: SearchResultsActivityViewModel

    private lateinit var binding: ActivitySearchResultsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchResultsActivityViewModel::class.java)

        val resultsItemAdapter = SearchResultsAdapter(this).apply {
            clickListener = object : RecyclerViewItemClickedListener<SearchResultItem> {
                override fun onItemClick(item: SearchResultItem) {

                }
            }
        }

        /* setup binding */
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_results)
        binding.let {
            it.lifecycleOwner = this
            it.viewModel = this.viewModel
            it.resultsListRecyclerView.apply {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@SearchResultsActivity)
                adapter = resultsItemAdapter
            }
        }

        /**
         * Get search parameters
         */
        val searchRequestParameters =
            intent.extras?.let {
                SearchRequestParameters(
                    it.getString(ORIGIN) ?: "",
                    it.getString(DESTINATION, ""),
                    it.getString(DEPARTURE_DATE, ""),
                    it.getInt(NUMBER_OF_ADULTS),
                    it.getInt(NUMBER_OF_TEEN),
                    it.getInt(NUMBER_OF_CHILDREN)
                )
            }

        Timber.d("searchRequestParameters: $searchRequestParameters")

        viewModel.search(searchRequestParameters ?: return).observe(this, Observer {
            Timber.d("search for : $searchRequestParameters response $it")

            val flights = it.trips.first().dates.flatMap { date ->
                date.flights.map { flight ->
                    SearchResultItem(
                        it.trips.first().dates.first().dateOut,
                        flight.flightKey,
                        flight.segments.first().duration,
                        flight.regularFare.fares.first().publishedFare.toString(),
                        it.currency
                    )
                }
            }

            Timber.d("Flights $flights")

            resultsItemAdapter.submitList(flights)
        })
    }

}