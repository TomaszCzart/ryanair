package com.ryanair.czart.ui.searchResultsActivity.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.ryanair.czart.data.RyanairRepository
import com.ryanair.czart.helpers.SearchRequestParameters
import com.ryanair.czart.network.models.FlightSearchResultsDto
import javax.inject.Inject

class SearchResultsActivityViewModel @Inject constructor(
    app: Application,
    private val ryanairRepository: RyanairRepository
) :
    AndroidViewModel(app) {

    /**
     * This field observes for any change of the stations
     * list from the server and transforms received result
     * to the list that will be displayed by a Spinner view.
     */
    val searchResults: MutableLiveData<FlightSearchResultsDto> = MutableLiveData()


    fun search(searchRequestParameters: SearchRequestParameters): MutableLiveData<FlightSearchResultsDto> =
        ryanairRepository.searchFlights(searchRequestParameters)

}
