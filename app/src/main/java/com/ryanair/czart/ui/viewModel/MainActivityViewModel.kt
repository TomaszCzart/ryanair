package com.ryanair.czart.ui.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.ryanair.czart.data.RyanairRepository
import com.ryanair.czart.helpers.SearchRequestParameters
import com.ryanair.czart.helpers.Station
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(app: Application, private val flightsRepository: RyanairRepository) :
    AndroidViewModel(app) {

    /**
     * This field observes for any change of the stations
     * list from the server and transforms received result
     * to the list that will be displayed by a Spinner view.
     */
    val stationsList: LiveData<List<Station>> = Transformations.map(flightsRepository.getAllStations()) { list ->
        list.map { Station(it.name, it.code) }
    }

    val originStation = MutableLiveData<Station>()
    val destinationStation = MutableLiveData<Station>()

    val numberOfAdults = MutableLiveData<Int>()
    val numberOfTeens = MutableLiveData<Int>()
    val numberOfChildren = MutableLiveData<Int>()

    val departureDate: String = ""

}