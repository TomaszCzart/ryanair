package com.ryanair.czart.ui.searchResultsActivity.recyclerView

data class SearchResultItem(
    val flightDate: String,
    val flightNumber: String,
    val duration: String,
    val regularFarePrice: String,
    val currency: String
)