package com.ryanair.czart.ui.searchResultsActivity.recyclerView

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ryanair.czart.databinding.SearchResultListItemBinding
import com.ryanair.czart.helpers.RecyclerViewItemClickedListener


class SearchResultsAdapter(context: Context) : ListAdapter<SearchResultItem, SearchResultsAdapter.MyViewHolder>(
    object : DiffUtil.ItemCallback<SearchResultItem>() {
        override fun areContentsTheSame(oldItem: SearchResultItem, newItem: SearchResultItem): Boolean {
            return oldItem.duration == newItem.duration && oldItem.regularFarePrice == newItem.regularFarePrice && oldItem.currency == newItem.currency
        }

        override fun areItemsTheSame(oldItem: SearchResultItem, newItem: SearchResultItem): Boolean =
            oldItem.flightNumber == newItem.flightNumber
    }
) {
    var clickListener: RecyclerViewItemClickedListener<SearchResultItem>? = null


    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = SearchResultListItemBinding.inflate(inflater, parent, false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class MyViewHolder(val binding: SearchResultListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SearchResultItem) {
            binding.item = item

            binding.root.setOnClickListener {
                clickListener?.onItemClick(item)
            }
        }
    }

}