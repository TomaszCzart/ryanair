package com.ryanair.czart.network

import com.ryanair.czart.network.models.FlightSearchResultsDto
import com.ryanair.czart.network.models.StationsDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface RyanairApiService {

    /**
     * Retrieve list of available stations (Airports).
     */
    @GET("static/stations.json")
    fun getAllStations(): Call<StationsDto>

    /**
     * Get search results for flights.
     *
     * @param searchRequest Search parameters object.
     */
    @GET("/api/v3/Availability?datein=&flexdaysbeforeout=3&flexdaysout=3&flexdaysbeforein=3&flexdaysin=3&roundtrip=false&ToUs=AGREED")
    fun getFlights(@QueryMap searchRequest: Map<String, String>): Call<FlightSearchResultsDto>

}