package com.ryanair.czart.network.models

/**
 * Main data class that keeps Stations list.
 */
data class StationsDto(
    val stations : List<StationDto>
)

/**
 * Station entity class.
 */
data class StationDto(
    val code: String,
    val name: String,
    val alternateName: String,
    val alias: List<String>,
    val countryCode: String,
    val countryName: String,
    val countryAlias: String?,
    val countryGroupCode: String,
    val countryGroupName: String,
    val timeZoneCode: String,
    val latitude: String,
    val longitude: String,
    val mobileBoardingPass: Boolean,
    val markets: List<Market>,
    val notices: String?
)

/* Market Data Class */
data class Market(
    val code: String,
    val group: String?
)