package com.ryanair.czart.network.models

data class FlightSearchResultsDto(
    val termsOfUse: String,
    val currency: String,
    val currPrecision: Int,
    val trips: List<TripDto>,
    val serverTimeUTC: String
)

data class TripDto(
    val origin: String,
    val originName: String,
    val destination: String,
    val destinationName: String,
    val dates: List<DateDto>
)

data class DateDto(
    val dateOut: String,
    val flights: List<FlightDto>
)

data class FlightDto(
    val faresLeft: Int,
    val flightKey: String,
    val infantsLeft: Int,
    val regularFare: RegularFareDto,
    val segments: List<SegmentDto>
)

data class RegularFareDto(
    val fareKey: String,
    val fareClass: String,
    val fares: List<FareDto>,
    val flightNumber: String,
    val time: List<String>,
    val timeUTC: List<String>,
    val duration: String
)

data class FareDto(
    val type: String,
    val amount: Float,
    val count: Int,
    val hasDiscount: Boolean,
    val publishedFare: Float,
    val discountInPercent: Int,
    val hasPromoDiscount: Boolean

)

data class SegmentDto(
    val segmentNr: Int,
    val origin: String,
    val destination: String,
    val flightNumber: String,
    val time: List<String>,
    val timeUTC: List<String>,
    val duration: String
)