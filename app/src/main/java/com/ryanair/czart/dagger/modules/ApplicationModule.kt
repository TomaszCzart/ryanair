package com.ryanair.czart.dagger.modules

import android.app.Application
import android.content.Context
import com.ryanair.czart.application.RyanairApplication
import com.ryanair.czart.dagger.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {
    @Provides
    @ApplicationScope
    fun provideContext(application: RyanairApplication): Context {
        return application.applicationContext
    }

    @Provides
    @ApplicationScope
    fun provideApplication(application: RyanairApplication): Application {
        return application
    }
}