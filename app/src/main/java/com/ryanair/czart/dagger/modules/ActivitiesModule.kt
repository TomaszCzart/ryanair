package com.ryanair.czart.dagger.modules

import androidx.lifecycle.ViewModelProvider
import com.ryanair.czart.ui.MainActivity
import com.ryanair.czart.dagger.scope.MainActivityScope
import com.ryanair.czart.dagger.scope.SearchResultsActivityScope
import com.ryanair.czart.ui.searchResultsActivity.SearchResultsActivity
import com.ryanair.czart.ui.viewModel.MainActivityViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {
    @MainActivityScope
    @ContributesAndroidInjector()
    abstract fun contributeMainActivityInjector(): MainActivity

    @SearchResultsActivityScope
    @ContributesAndroidInjector()
    abstract fun contributeSearchResultsActivityInjector(): SearchResultsActivity
}