package com.ryanair.czart.dagger.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class ApplicationScope