package com.ryanair.czart.dagger.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ryanair.czart.dagger.scope.ApplicationScope
import com.ryanair.czart.network.RyanairApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@Module(includes = [(ApplicationModule::class)])
class NetworkModule {

    @Provides
    @ApplicationScope
    fun ryanairApiService(retrofit: Retrofit): RyanairApiService {
        return retrofit.create(RyanairApiService::class.java)
    }

    @Provides
    @ApplicationScope
    fun gson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @ApplicationScope
    fun retrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .baseUrl("https://tripstest.ryanair.com/")
            .build()
    }

    @Provides
    @ApplicationScope
    fun okHttpClient(): OkHttpClient {
        return OkHttpClient.Builder().build()
    }
}