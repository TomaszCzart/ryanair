package com.ryanair.czart.dagger

import com.ryanair.czart.application.RyanairApplication
import com.ryanair.czart.dagger.modules.ActivitiesModule
import com.ryanair.czart.dagger.modules.ApplicationModule
import com.ryanair.czart.dagger.modules.MainActivityModule
import com.ryanair.czart.dagger.modules.NetworkModule
import com.ryanair.czart.dagger.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

@ApplicationScope
@Component(modules = [AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    ActivitiesModule::class,
    MainActivityModule::class,
    NetworkModule::class])
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: RyanairApplication): Builder

        fun build(): ApplicationComponent
    }

    fun inject(application: RyanairApplication)
}