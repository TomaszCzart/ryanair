package com.ryanair.czart.data

import androidx.lifecycle.MutableLiveData
import com.ryanair.czart.helpers.SearchRequestParameters
import com.ryanair.czart.network.models.FlightSearchResultsDto
import com.ryanair.czart.network.models.StationDto
import javax.inject.Inject

/**
 * This class provides abstraction layer for api requests.
 */
class RyanairRepository @Inject constructor(private val ryanairDao: RyanairDao) {
    private val allStations: MutableLiveData<List<StationDto>> = ryanairDao.loadAllStations()

    fun getAllStations(): MutableLiveData<List<StationDto>> = allStations

    fun searchFlights(searchRequestParameters: SearchRequestParameters): MutableLiveData<FlightSearchResultsDto> =
        ryanairDao.searchFlights(searchRequestParameters)
}