package com.ryanair.czart.data

import androidx.lifecycle.MutableLiveData
import com.ryanair.czart.helpers.SearchRequestParameters
import com.ryanair.czart.network.models.FlightSearchResultsDto
import com.ryanair.czart.network.models.StationDto

interface IRyanairDao {
    /**
     * Returns all stations
     */
    fun loadAllStations(): MutableLiveData<List<StationDto>>

    /**
     * Get search results for flights.
     *
     * @param searchRequest Search parameters object.
     */
    fun searchFlights(searchRequestParameters: SearchRequestParameters): MutableLiveData<FlightSearchResultsDto>
}