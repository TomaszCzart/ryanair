package com.ryanair.czart.data

import androidx.lifecycle.MutableLiveData
import com.ryanair.czart.helpers.SearchRequestParameters
import com.ryanair.czart.network.RyanairApiService
import com.ryanair.czart.network.models.FlightSearchResultsDto
import com.ryanair.czart.network.models.StationDto
import com.ryanair.czart.network.models.StationsDto
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject

class RyanairDao @Inject constructor(private val ryanairApiService: RyanairApiService) : IRyanairDao {

    private val stations = MutableLiveData<List<StationDto>>()

    /**
     * Get all stations and post it to the live data.
     * If request fail, there will be nothing returned.
     *
     * @return Live data that will stream stations list on response.
     */
    override fun loadAllStations(): MutableLiveData<List<StationDto>> {

        ryanairApiService.getAllStations().enqueue(object : Callback<StationsDto> {
            override fun onResponse(call: Call<StationsDto>, response: Response<StationsDto>) {
                Timber.d("Response ${response.body()?.stations}")
                stations.postValue(response.body()?.stations)
            }

            override fun onFailure(call: Call<StationsDto>, t: Throwable) {
                //TODO: failure handling
            }
        })

        return stations
    }

    /**
     * Search flights by given parameters.
     *
     * @return Live data that will stream search results list on response.
     */
    override fun searchFlights(searchRequestParameters: SearchRequestParameters): MutableLiveData<FlightSearchResultsDto> {
        val searchResults = MutableLiveData<FlightSearchResultsDto>()

        /**
         * Map parameters to API request format.
         */
        val searchRequest = HashMap<String, String>().apply {
            put("origin", searchRequestParameters.originStation)
            put("destination", searchRequestParameters.destinationStation)
            put("dateout", searchRequestParameters.departureDate)
            put("adt", searchRequestParameters.numberOfAdults.toString())
            put("teen", searchRequestParameters.numberOfTeens.toString())
            put("chd", searchRequestParameters.numberOfChildren.toString())
        }

        ryanairApiService.getFlights(searchRequest).enqueue(object : Callback<FlightSearchResultsDto> {
            override fun onResponse(call: Call<FlightSearchResultsDto>, response: Response<FlightSearchResultsDto>) {
                Timber.d("Response ${response.body()?.trips}")
                searchResults.postValue(response.body() ?: return)
            }

            override fun onFailure(call: Call<FlightSearchResultsDto>, t: Throwable) {
                //TODO: failure handling
            }
        })

        return searchResults
    }

}