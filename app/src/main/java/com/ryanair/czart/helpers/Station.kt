package com.ryanair.czart.helpers

/**
 * Data class for properly display stations in a Spinner.
 */
data class Station(val name: String, val code: String) {
    /* Result of the toString will be visible on a Spinner dropdown. */
    override fun toString(): String {
        return "$name ($code)"
    }
}