package com.ryanair.czart.helpers

interface RecyclerViewItemClickedListener<E> {
    fun onItemClick(item: E)
}