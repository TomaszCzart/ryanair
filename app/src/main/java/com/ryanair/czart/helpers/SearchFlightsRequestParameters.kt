package com.ryanair.czart.helpers

import androidx.annotation.StringDef

@StringDef(ORIGIN, DESTINATION, DEPARTURE_DATE, NUMBER_OF_ADULTS, NUMBER_OF_TEEN, NUMBER_OF_CHILDREN)
@Retention(AnnotationRetention.SOURCE)
annotation class SearchFlightsRequestParameters

const val ORIGIN = "origin"
const val DESTINATION = "destination"
const val DEPARTURE_DATE = "dateout"
const val NUMBER_OF_ADULTS = "adt"
const val NUMBER_OF_TEEN = "teen"
const val NUMBER_OF_CHILDREN = "chd"