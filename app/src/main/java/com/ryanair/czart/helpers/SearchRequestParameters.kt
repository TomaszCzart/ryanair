package com.ryanair.czart.helpers

data class SearchRequestParameters(
    val originStation: String,
    val destinationStation: String,
    val departureDate: String,
    val numberOfAdults: Int,
    val numberOfTeens: Int,
    val numberOfChildren: Int
)